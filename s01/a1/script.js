// Create a new class called Contractor
// A contractor has the following properties: name, email, contactNo
// A contractor also has a method called getContractorDetails and displays the name, email, and contactNo of the contractor


class Contractor{
    constructor(name, email, contactNo){
        this.name = name;
        this.email = email;
        this.contactNo = contactNo;
    }
    getContractorDetails(){
        console.log(`Name: ${this.name}`);
        console.log(`Email: ${this.email}`);
        console.log(`Contact No: ${this.contactNo}`);
    }
}
let contractor1 = new Contractor('Ultra Manpower Services', 'ultra@manpower.com', '09147830123');
contractor1.getContractorDetails();

class Subcontractor extends Contractor{
    constructor(name, email, contactNo, specialization){
        super(name, email, contactNo);
        this.specialization = specialization;
    }
    getSubConDetails(){
        this.getContractorDetails();
        console.log(`${this.specialization}`);
    }
}

let subCon1 = new Subcontractor('Ace Bros', 'acebros@mail.com', '12345678', ['gardens', 'industrial']);
let subCon2 = new Subcontractor('LitC Corp', 'litc@mail.com', '125436745784', ['schools', 'hospitals', 'bakeries','libraries']);

subCon1.getSubConDetails();
subCon2.getSubConDetails();
