
class RegularShape{
    #noSides;
    #length;
    constructor(noSides, length){
        this.#noSides = noSides;
        this.#length = length;
        if(this.constructor === RegularShape){
			throw new Error(
				"Class must implement login() method."
			);
		}
        if(this.getPerimeter() === undefined){
			throw new Error(
				"Class must implement register() method."
			);
		}
        if(this.getArea() === undefined){
			throw new Error(
				"Class must implement register() method."
			);
		}
    }
    getPerimeter(){
        return `The perimeter of the square is `;
    }
    getArea(){
        return `The area is `;
    }
}
class Triangle extends RegularShape{
    constructor(noSides, length){
        super(noSides,length)
    }
    getPerimeter(){
        return super.getPerimeter() + `${this.length *3} `
    }

    getArea(){
        return super.getArea() + `${this.length * this.length * Math.sqrt(3) / 4}`;
    }
    setNoSides(noSides){
        this.noSides = noSides;
    }
    setLength(length){
        this.length = length;
    }
    getLength(){
        return this.length;
    }

}
class Square extends RegularShape{
    constructor(noSides, length){
        super(noSides, length);
    }
    getPerimeter(){
        return super.getPerimeter() + `${this.length *4} `
    }
    getArea(){
        return super.getArea() + `${this.length * this.length}`;
    }
    setNoSides(noSides){
        this.noSides = noSides;
    }
    setLength(length){
        this.length = length;
    }
    getLength(){
        return this.length;
    }
    
}


let triangle1 = new Triangle();
triangle1.setNoSides(3);
triangle1.setLength(15);
console.log(triangle1.getLength());
console.log(triangle1.getArea()); //Bonus
let square1 = new Square();
square1.setNoSides(4);
square1.setLength(65);
console.log(square1.noSides);
console.log(square1.getLength());
console.log(square1.getPerimeter());



class User {
    constructor(){
        if(this.login === undefined){
			throw new Error(
				"Class must implement login() method."
			);
		}
        if(this.register === undefined){
			throw new Error(
				"Class must implement register() method."
			);
		}
        if(this.logout === undefined){
			throw new Error(
				"Class must implement logout() method."
			);
		}
    }
}

class RegularUser extends User{
    #name;
    #email;
    #password;
    constructor(){
        super()
       
    }
    login(){
        return `${this.#name} has logged in.`;
    }
    register(){
        return `${this.#name} has registered.`
    }
    logout(){
        return `${this.#name} has logged out.`
    }
    browseJobs(){
        return `There are 10 jobs found.`
    }
    setName(name){
        this.#name = name;
    }
    setEmail(email){
        this.#email = email;
    }
    setPassword(password){
        this.#password = password;
    }
}
const regUser1 = new RegularUser();
regUser1.setName('Dan');
regUser1.setEmail('dan@mail.com');
regUser1.setPassword('Dan12345');
console.log(regUser1.register());
console.log(regUser1.login());
console.log(regUser1.browseJobs());
console.log(regUser1.logout());


class Admin extends User{
    #name;
    #email;
    #password;
    #hasAdminExpired;
    constructor(name, email, password, hasAdminExpired){
        super()
        this.#name = name;
        this.#email = email;
        this.#password = password;
        this.#hasAdminExpired = hasAdminExpired;
    }
    login(){
        return `Admin ${this.#name} has logged in.`;
    }
    register(){
        return `Admin ${this.#name} has registered.`
    }
    logout(){
        return `Admin ${this.#name} has logged out.`
    }
    postJob(){
        return `Job posting added to site.`
    }
    setName(name){
        this.#name = name;
    }
    setEmail(email){
        this.#email = email;
    }
    setPassword(password){
        this.#password = password;
    }
    setHasAdminExpired(b){
        this.#hasAdminExpired = b;
    }
    getHasAdminExpired(b){
        return this.#hasAdminExpired;
    }
}
const admin = new Admin();
admin.setName('Joe');
admin.setEmail('joe@mail.com');
admin.setPassword('joe12345');
admin.setHasAdminExpired(false);
console.log(admin.register());
console.log(admin.login());
console.log(admin.getHasAdminExpired());
console.log(admin.postJob());
console.log(admin.logout());