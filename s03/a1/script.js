
class RegularShape {
    constructor() {
        if (this.constructor === RegularShape) {
            throw new Error(
                'You are not allowed to create a object using the RegularShape class'
            );
        }
        if (this.getPerimeter() === undefined) {
            throw new Error(
                "Class must implement getPerimeter"
            )
        }
        if (this.getArea() === undefined) {
            throw new Error(
                "Class must implement getArea"
            )
        }
    }
}

class Square extends RegularShape {
    constructor(noSLides, length) {
        super();
        this.noSLides = noSLides;
        this.length = length;
    }
    getPerimeter() {
        return `The peremieter of the square is ${this.length * 4} `
    }
    getArea() {
        return `The area of the square is ${this.length * this.length}`;
    }

}
let shape1 = new Square(4, 16);
console.log(shape1.getPerimeter());
console.log(shape1.getArea());

class Food {
    constructor(name,price) {
        this.name = name;
        this.price = price;

        if (this.constructor === Food) {
            throw new Error(
                'You are not allowed to create a object using the Food class'
            );
        }
        if (this.getName() === undefined) {
            throw new Error(
                "Class must implement getName"
            )
        }
        
    }
}

class Vegetable extends Food {
    constructor(name, breed, price) {
        super(name,price);
        this.breed = breed;
    }
    getName() {
        return `${this.name} is of ${this.breed} variety and is priced at ${this.price} pesos. `
    }
}
const vegetable1 = new Vegetable('Pechay', 'Native','25');
console.log(vegetable1.getName());





class Equipment {
    constructor() {
        
        if (this.constructor === Equipment) {
            throw new Error(
                'You are not allowed to create a object using the Equipment class'
            );
        }
        if (this.printInfo() === undefined) {
            throw new Error(
                "Class must implement printInfo"
            )
        }
        
    }
}
class Bulldozer extends Equipment{
    constructor(equipmentType, model, bladeType){
        super();
        this.equipmentType = equipmentType;
        this.model = model;
        this.bladeType = bladeType;
    }
    printInfo(){
        return `Info: ${this.equipmentType}
The bulldozer ${this.model} has a ${this.bladeType} blade`
    }
}

let bulldozer1 = new Bulldozer('bulldozer', 'Brute', 'Shovel');
console.log(bulldozer1.printInfo());

class TowerCrane extends Equipment{
    constructor(equipmentType, model, hookRadius, maxCapacity){
        super();
        this.equipmentType = equipmentType;
        this.model = model;
        this.hookRadius = hookRadius;
        this.maxCapacity = maxCapacity;
    }
    printInfo(){
        return `Info: ${this.equipmentType}
The tower crane ${this.model} has ${this.hookRadius}cm hook radius and ${this.maxCapacity}kg max capacity`

;
    }
}
let towercrane1 = new TowerCrane('tower crane','Pelican','100', '1500');
console.log(towercrane1.printInfo());

