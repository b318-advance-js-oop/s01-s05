class Equipment {
    constructor(equipmentType){
        this.equipmentType = equipmentType;
    }
    printInfo(){
        return `Info: ${this.equipmentType}`;
    }
}
class Bulldozer extends Equipment{
    constructor(equipmentType, model, bladeType){
        super(equipmentType);
        this.model = model;
        this.bladeType = bladeType;
    }
    printInfo(){
        return super.printInfo() +` 
The bulldozer ${this.model} has a ${this.bladeType} blade`
    }
}

let bulldozer1 = new Bulldozer('bulldozer', 'Brute', 'Shovel');
console.log(bulldozer1.printInfo());

class TowerCrane extends Equipment{
    constructor(equipmentType, model, hookRadius, maxCapacity){
        super(equipmentType);
        this.model = model;
        this.hookRadius = hookRadius;
        this.maxCapacity = maxCapacity;
    }
    printInfo(){
        return super.printInfo() + `
The tower crane ${this.model} has ${this.hookRadius}cm hook radius and ${this.maxCapacity}kg max capacity`

;
    }
}
let towercrane1 = new TowerCrane('tower crane','Pelican','100', '1500');
console.log(towercrane1.printInfo());

class BackLoader extends Equipment{
    constructor(equipmentType,model,type, tippingLoad){
        super(equipmentType);
        this.model = model;
        this.type = type;
        this.tippingLoad = tippingLoad;
    }
    printInfo(){
        return super.printInfo() + `
the loader ${this.model} is a ${this.type} loader and has a tipping load of ${this.tippingLoad}lbs.`
    }
}
let backLoader1 = new BackLoader('back loader', 'Turtle','hydraulic','1500');
console.log(backLoader1.printInfo());

class RegularShape{
    constructor(noSLides, length){
        this.noSLides = noSLides;
        this.length = length;
    }
    getPerimeter(){
        return `The perimeter is `;
    }
    getArea(){
        return `The area is `;
    }
}
class Triangle extends RegularShape{
    constructor(noSLides, length){
        super(noSLides,length)
    }
    getPerimeter(){
        return super.getPerimeter() + `${this.length *3} `
    }

    getArea(){
        return super.getArea() + `${this.length * this.length * Math.sqrt(3) / 4}`;
    }

}
class Square extends RegularShape{
    constructor(noSLides, length){
        super(noSLides, length);
    }
    getPerimeter(){
        return super.getPerimeter() + `${this.length *4} `
    }
    getArea(){
        return super.getArea() + `${this.length * this.length}`;
    }
    
}


let triangle1 = new Triangle(3, 10);
console.log(triangle1.getPerimeter());
console.log(triangle1.getArea()); //Bonus
let square1 = new Square(4, 12);
console.log(square1.getPerimeter());
console.log(square1.getArea());
