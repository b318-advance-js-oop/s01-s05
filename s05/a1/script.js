// Capstone Instructions:

// 1. Create a Request class that has the following properties:
// requesterEmail (string - email of Employee who created request)
// content (string)
// dateRequested (JS Date object)

class Request {
    constructor(requesterEmail, content, dateRequested) {
        this.requesterEmail = requesterEmail;
        this.content = content;
        this.dateRequested = new Date(dateRequested);
    }
}

class Person {
    #firstName;
    #lastName;
    #email;
    #department;
    constructor(firstName, lastName, email, department) {
        this.#firstName = firstName;
        this.#lastName = lastName;
        this.#email = email;
        this.#department = department;

        if (this.constructor === Person) {
            throw new Error(
                'You are not allowed to create a object using the Person class'
            );
        }
        if (this.getFullName() === undefined) {
            throw new Error(
                "Class must implement getFullName"
            )
        }
        if (this.login() === undefined) {
            throw new Error(
                "Class must implement login"
            )
        }
        if (this.logout() === undefined) {
            throw new Error(
                "Class must implement logout"
            )
        }
    }
    getFullName() {

    }
}

class Employee extends Person {
    #isActive;
    #requests;
    constructor(firstName, lastName, email, department) {
        super(firstName, lastName, email, department);
        this.#isActive = true
        this.#requests = [];
    }
    setFirstName(name) {
        this.firstName = name;
    }
    setLastName(last) {
        this.lastName = last;
    }
    setEmail(email) {
        this.email = email;
    }
    setDepartment(dept) {
        this.department = dept;
    }
    setIsActive(boolean) {
        this.#isActive = boolean;
    }
    setRequests(any) {
        this.#requests.push({...any, isActive: this.getIsActive(), department: this.getDepartment(), firstName: this.getFirstName(), lastName: this.getLastName()});
    }
    getFirstName() {
        return this.firstName;
    }
    getLastName() {
        return this.lastName;
    }
    getEmail() {
        return this.email;
    }
    getDepartment() {
        return this.department;
    }
    getIsActive() {
        return this.#isActive;
    }
    getRequests() {
        return this.#requests;
    }
    getFullName() {
        return `${this.firstName} ${this.lastName} is my full name.`
    }
    login() {
        return `${this.firstName} has logged in.`;
    }
    logout() {
        return `${this.firstName} has logged out.`
    }
    addRequest(content, dateRequested) {
        const newRequest = new Request(this.email, content, dateRequested);
        this.setRequests(newRequest);
        // const allMatchingEmail = this.#requests.filter((employee) => employee.requesterEmail === this.email)
        // console.log(allMatchingEmail)
    }
}
const employee = new Employee();
employee.setFirstName('joe');
employee.setLastName('gay');
employee.setEmail('joe@mail.com')
employee.setDepartment('ABC')
employee.addRequest('pro', '2019-2-5')
console.log(employee.getRequests())

employee.setFirstName('kyle');
employee.setLastName('ross');
employee.setEmail('kyle@mail.com')
employee.setDepartment('DEF')
employee.addRequest('gamer', '2017-2-5')
console.log(employee.getRequests())

employee.setFirstName('jez');
employee.setLastName('flick');
employee.setEmail('jez@mail.com')
employee.setDepartment('GHI')
employee.addRequest('solo', '2011-2-5')
console.log(employee.getRequests())

class Teamlead extends Person {
    #isActive;
    #members;
    constructor(firstName, lastName, email, department) {
        super(firstName, lastName, email, department);
        this.#isActive = true
        this.#members = [];
    }
    setFirstName(name) {
        this.firstName = name;
    }
    setLastName(last) {
        this.lastName = last;
    }
    setEmail(email) {
        this.email = email;
    }
    setDepartment(dept) {
        this.department = dept;
    }
    setIsActive(boolean) {
        this.#isActive = boolean;
    }
    setMembers(any) {
        this.#members.push({...any, isActive: this.#isActive});
    }
    getFirstName() {
        return this.firstName;
    }
    getLastName() {
        return this.lastName;
    }
    getEmail() {
        return this.email;
    }
    getDepartment() {
        return this.department;
    }
    getIsActive() {
        return this.#isActive;
    }
    getMembers() {
        return this.#members;
    }
    getFullName() {
        return `${this.firstName} ${this.lastName} is my full name.`
    }
    login() {
        return `${this.firstName} has logged in.`;
    }
    logout() {
        return `${this.firstName} has logged out.`
    }
    addMember(employee) {
        this.#members.push(...employee);
    }

    checkRequests(employeeEmail) {
        const allMatchingEmail = this.#members.find((employee) => employee.requesterEmail === employeeEmail)
        if (allMatchingEmail) {
            return allMatchingEmail;
        } else {
            return "No matches are found.";
        }
    }
    addRequest(content, dateRequested) {
        const newRequest = new Request(this.email, content, dateRequested);
        this.addMember([{requesterEmail: newRequest.requesterEmail, content: newRequest.content, dateRequested: newRequest.dateRequested, isActive: this.getIsActive(), department: this.getDepartment(), firstName: this.getFirstName(), lastName: this.getLastName()}]);
        console.log(newRequest)
    }
}

const teamlead = new Teamlead();
teamlead.setFirstName('team1');
teamlead.setLastName('lead1');
teamlead.setEmail('team@mail.com')
teamlead.setDepartment('TM')
teamlead.addRequest('tiktok', '2023-4-1')

teamlead.addMember(employee.getRequests());
console.log(teamlead.checkRequests('kyle@mail.com'));
console.log(teamlead.getMembers());

class Admin extends Person {
    #teamLeads;
    constructor(firstName, lastName, email, department) {
        super(firstName, lastName, email, department);
        this.#teamLeads = [];
    }
    setFirstName(name) {
        this.firstName = name;
    }
    setLastName(last) {
        this.lastName = last;
    }
    setEmail(email) {
        this.email = email;
    }
    setDepartment(dept) {
        this.department = dept;
    }
    setTeamleads(any) {
        this.#teamLeads.push(...any);
    }
    getFirstName() {
        return this.firstName;
    }
    getLastName() {
        return this.lastName;
    }
    getEmail() {
        return this.email;
    }
    getDepartment() {
        return this.department;
    }
    getTeamleads() {
        return this.#teamLeads;
    }
    getFullName() {
        return `${this.firstName} ${this.lastName} is my full name.`
    }
    login() {
        return `${this.firstName} has logged in.`;
    }
    logout() {
        return `${this.firstName} has logged out.`
    }
    addTeamlead(employee) {
        this.#teamLeads.push(...employee);
    }

    deactivateTeam(employeeEmail) {
        const allMatchingEmail = this.#teamLeads.find((employee) =>{
            return employee.requesterEmail === employeeEmail
        } )
        console.log(allMatchingEmail)
        if (allMatchingEmail) {
            console.log(allMatchingEmail)
            return allMatchingEmail.isActive = false;
        } else {
            return "No matches are found.";
        }
    }
}

const admin = new Admin();
admin.setFirstName('admin1');
admin.setLastName('Min');
admin.setEmail('admin@mail.com')
admin.setDepartment('ADS')
admin.addTeamlead(teamlead.getMembers());

console.log(admin.deactivateTeam('jez@mail.com'));
console.log(admin.getTeamleads())




